# Dokumentasi Demo Aplikasi Tes Praktik

## Debug Aplikasi:
https://drive.google.com/file/d/1WVRGNnXuTWGOppCzRoT7OYPDuTCbfC3E/view?usp=sharing

### 1: Tampilan Login
![ss_login](https://github.com/JonathanUlhaq/Chatbot-WebView/assets/73418941/42b074eb-a4f4-460f-aa85-1603b0224657) 

### 2: Tampilan Account
![ss_account](https://github.com/JonathanUlhaq/Chatbot-WebView/assets/73418941/5cff6b83-6f2b-4302-a9a0-624fccae1e66)

### 3: Tampilan Joblist
![ss_joblist](https://github.com/JonathanUlhaq/Chatbot-WebView/assets/73418941/624d20fd-e7b3-4f8b-917f-668151e833dc)

### 4: Tampilan Joblist
![ss_list_filter](https://github.com/JonathanUlhaq/Chatbot-WebView/assets/73418941/ee4086c0-b108-4150-b3a7-7f22b0817f3c)

### 5: Tampilan Detail Job
![ss_detail_job](https://github.com/JonathanUlhaq/Chatbot-WebView/assets/73418941/e3a8a40e-3445-4384-8da9-76e9c6149fc1)




