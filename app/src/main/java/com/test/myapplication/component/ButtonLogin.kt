package com.test.myapplication.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ButtonLogin(
    color:Color,
    icon:Int,
    text:String,
    fontColor:Color,
    fraction:Float = 1f,
    onClick:() -> Unit
) {
    Button(
        onClick = { onClick.invoke()},
        modifier = Modifier
            .fillMaxWidth(fraction),
        shape = RoundedCornerShape(12.dp),
        colors = ButtonDefaults.buttonColors(
            containerColor = color
        ),
        elevation = ButtonDefaults.buttonElevation(6.dp)
    ) {
        Image(painter = painterResource(id = icon),
            contentDescription = null,
            modifier = Modifier
                .size(14.dp))
        Spacer(modifier = Modifier.width(12.dp))
        Text(text = text,
            fontSize = 12.sp,
            color = fontColor)
    }
}