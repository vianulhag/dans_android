package com.test.myapplication.component

import android.util.Log
import androidx.compose.foundation.text.ClickableText
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle

@Composable
fun HyperLink(url: String) {
    val annotatedString = buildAnnotatedString {

        pushStringAnnotation("website", annotation = url)
        withStyle(
            style = SpanStyle(
                color = Color(0xFF0088FF),
                textDecoration = TextDecoration.Underline
            )
        ) {
            append("Go To Website")
        }
        pop()
    }
    ClickableText(text = annotatedString, onClick = {
        annotatedString.getStringAnnotations(tag = "website", start = it, end = it).firstOrNull()?.let {
            Log.d("web url", it.item)
        }
    } )
}