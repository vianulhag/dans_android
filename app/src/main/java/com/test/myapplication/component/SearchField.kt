package com.test.myapplication.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import com.test.myapplication.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchField(
    value:String,
    onValueChange:MutableState<String>,
    width:Float = 0.9f
) {
    OutlinedTextField(value = value ,
        onValueChange = {onValueChange.value = it.lowercase()},
        leadingIcon = {
            Icon(painter = painterResource(id = R.drawable.search_icon) ,
                contentDescription = null )
        },
        modifier = Modifier
            .fillMaxWidth(width),
        singleLine = true,
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
        shape = RoundedCornerShape(12.dp)
    )
}