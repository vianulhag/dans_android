package com.test.myapplication.component

import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun PhotoProfileWrapper(photo:@Composable () -> Unit) {
    Surface(
        shape = CircleShape,
        modifier = Modifier
            .size(100.dp)
    ) {
        photo.invoke()
    }
}