package com.test.myapplication.repositories

import com.test.myapplication.network.response.DetailJobResponse
import com.test.myapplication.network.service.JobAPI
import javax.inject.Inject

class DetailJobRepository @Inject constructor(private val api:JobAPI) {
    suspend fun getDetailJob(id:String):DetailJobResponse = api.getDetailJob(id)
}