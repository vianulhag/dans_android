package com.test.myapplication.repositories

import com.test.myapplication.network.response.ListJobResponse
import com.test.myapplication.network.service.JobAPI
import kotlinx.coroutines.delay
import javax.inject.Inject

class ListJobRepository @Inject constructor(private val api: JobAPI) {
    suspend fun getListJob(
        page: Int,
        full_time: Boolean = true,
        location: String,
        description: String
    ): List<ListJobResponse> {
        return api.getListJob(page, full_time, location, description)
    }

}