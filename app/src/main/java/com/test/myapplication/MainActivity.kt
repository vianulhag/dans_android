package com.test.myapplication

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.facebook.CallbackManager
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.test.myapplication.navigation.BottomnNavigationCustom
import com.test.myapplication.navigation.NavRoute
import com.test.myapplication.navigation.NavigationHost
import com.test.myapplication.ui.theme.Dans_androidTheme
import com.test.myapplication.utils.Const
import com.test.myapplication.view.login.LoginView
import com.test.myapplication.view.login.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var loginVm: LoginViewModel
    private lateinit var callbackManager: CallbackManager
    private lateinit var navController: NavHostController

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this@MainActivity)
        FacebookSdk.sdkInitialize(applicationContext)
        callbackManager = CallbackManager.Factory.create()
        auth = FirebaseAuth.getInstance()


        setContent {
            Dans_androidTheme {
                // A surface container using the 'background' color from the theme
                loginVm = hiltViewModel<LoginViewModel>()
                navController = rememberNavController()

                val showBackground = rememberSaveable {
                    mutableStateOf(true)
                }

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                   Scaffold (
                       bottomBar = {
                           if (showBackground.value) {
                               BottomnNavigationCustom(navController = navController)
                           }
                       }
                   ) {
                       Surface(
                           Modifier
                               .padding(it)
                       ) {
                           NavigationHost(
                               activity = this,
                               auth,
                               callbackManager,
                               navController,
                               showBackground
                           )
                       }
                   }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Const.RC_SIGN_IN.toInt()) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                loginVm.firebaseAuthWithGoogle(auth, account?.idToken, this) {
                    navController.navigate(NavRoute.Profile.route) {
                        popUpTo(navController.graph.id) {
                            inclusive = true
                        }
                    }
                }
            } catch (e: ApiException) {
                Log.e("ERROR SIGNIN KE GOOGLE", e.toString())
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Dans_androidTheme {
        Greeting("Android")
    }
}