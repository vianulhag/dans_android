package com.test.myapplication.widgets

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.test.myapplication.R
import com.test.myapplication.component.HyperLink
import com.test.myapplication.network.response.DetailJobResponse
import com.test.myapplication.network.response.ListJobResponse

@Composable
fun CompanyDetail(item: DetailJobResponse) {
    Surface (
        shape = RoundedCornerShape(12.dp),
        shadowElevation = 1.dp,
        border = BorderStroke(2.dp, Color(0xFFE6E6E6)),
        modifier = Modifier
            .fillMaxWidth()
            .height(100.dp)
    ) {
        Row (
            Modifier
                .padding(12.dp)
                .fillMaxHeight()
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Surface(
                Modifier
                    .size(80.dp),
                color =  Color(0xFFC9C9C9),
                shape = RoundedCornerShape(10.dp)
            ) {
                AsyncImage(model = ImageRequest
                    .Builder(LocalContext.current)
                    .data(item?.company_logo ?: "")
                    .crossfade(true)
                    .build(), contentDescription = null,
                    contentScale = ContentScale.Crop)
            }
            Spacer(modifier = Modifier.width(10.dp))
            Column {
                Text(text = item.company ?: "",
                    fontWeight = FontWeight.Bold,
                    fontSize = 14.sp,
                    maxLines = 1,
                    modifier = Modifier
                        .width(300.dp))
                Spacer(modifier = Modifier.height(4.dp))
                Text(text = item.location ?: "",
                    fontWeight = FontWeight.Normal,
                    fontSize = 12.sp,
                    maxLines = 1,
                    modifier = Modifier
                        .width(300.dp))
                Spacer(modifier = Modifier.height(10.dp))
                HyperLink(url = item.company_url ?: "")
            }
        }
    }
}