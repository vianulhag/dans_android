package com.test.myapplication.widgets

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.test.myapplication.network.response.DetailJobResponse

@Composable
fun JobSpecification(item: DetailJobResponse) {
    val scrollState = rememberScrollState()
    Surface (
        shape = RoundedCornerShape(12.dp),
        shadowElevation = 1.dp,
        border = BorderStroke(2.dp, Color(0xFFE6E6E6)),
        modifier = Modifier
            .fillMaxWidth()
        ) {
        Column (
            Modifier
                .padding(12.dp)
                .verticalScroll(scrollState)
        ) {
            Text(text = "Title",
                fontSize = 12.sp,
                color = Color(0xFFCCCCCC),
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.height(6.dp))
            Text(text = item.title ?: "",
                fontSize = 12.sp,
                color = Color(0xFF000000)
            )

            Spacer(modifier = Modifier.height(12.dp))
            Text(text = "Type",
                fontSize = 12.sp,
                color = Color(0xFFCCCCCC),
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.height(6.dp))
            Text(text = item.type ?: "",
                fontSize = 12.sp,
                color = Color(0xFF000000)
            )

            Spacer(modifier = Modifier.height(12.dp))
            Text(text = "Description",
                fontSize = 12.sp,
                color = Color(0xFFCCCCCC),
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.height(6.dp))
            Text(text = item.description ?: "",
                fontSize = 12.sp,
                color = Color(0xFF000000)
            )
        }
    }
}