package com.test.myapplication.utils

object Const {
    const val WEB_CLIENT_ID:String = "1070416301679-j9p0tb8p62jf8a2ipstr5p4b20mulu5l.apps.googleusercontent.com"
    const val RC_SIGN_IN:String = "0809"
    const val BASE_URL:String = "https://dev6.dansmultipro.com/api/recruitment/"
}