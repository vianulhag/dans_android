package com.test.myapplication.utils

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext

class CustSharePrefs(@ApplicationContext private val context: Context) {

    private val prefs: SharedPreferences =
        context.getSharedPreferences("LOGIN_DATA", Context.MODE_PRIVATE)


    fun saveName(name: String?) {
        val editor = prefs.edit()
        editor.putString("LOGIN_NAME", name)
        editor.apply()
    }

    fun getName():String = prefs.getString("LOGIN_NAME","") ?: ""

}