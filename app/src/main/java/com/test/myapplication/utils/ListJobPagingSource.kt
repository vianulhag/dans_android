package com.test.myapplication.utils

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.test.myapplication.network.response.ListJobResponse
import com.test.myapplication.repositories.ListJobRepository
import retrofit2.HttpException
import java.io.IOException

class ListJobPagingSource(
    private val repo: ListJobRepository,
    private val description: String,
    private val full_time: Boolean,
    private val location: String,
) : PagingSource<Int, ListJobResponse>() {

    override fun getRefreshKey(state: PagingState<Int, ListJobResponse>): Int? {
        return state.anchorPosition?.let { position ->
            val page = state.closestPageToPosition(position)
            page?.prevKey?.minus(1)?: page?.nextKey?.plus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ListJobResponse> {
        return try {
            val page = params.key ?: 1
            val response = repo.getListJob(page, full_time, location, description)
            LoadResult.Page(
                data = response,
                prevKey = null,
                nextKey = if (response.isEmpty()) null else page.plus(1)
            )
        } catch (e: IOException) {
            Log.e("Error ga bisa muat",e.message.toString())
            LoadResult.Error(e)

        } catch (e: HttpException) {
            Log.e("Error ga bisa muat",e.message.toString())
            LoadResult.Error(e)

        }
    }
}