package com.test.myapplication.di

import android.content.Context
import com.test.myapplication.network.service.JobAPI
import com.test.myapplication.utils.Const
import com.test.myapplication.utils.CustSharePrefs
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class Module {

    @Provides
    @Singleton
    fun clientProvider(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

    @Singleton
    @Provides
    fun sharePrefsProvider(@ApplicationContext context:Context):CustSharePrefs = CustSharePrefs(context)

    @Singleton
    @Provides
    fun retrofitProvider(client:OkHttpClient):JobAPI =
        Retrofit.Builder()
            .baseUrl(Const.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(JobAPI::class.java)


}