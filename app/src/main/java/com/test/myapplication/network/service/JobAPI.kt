package com.test.myapplication.network.service

import com.test.myapplication.network.response.DetailJobResponse
import com.test.myapplication.network.response.ListJobResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface JobAPI {

    @GET("positions.json")
    suspend fun getListJob(
        @Query("page")page:Int,
        @Query("full_time")full_time:Boolean = true,
        @Query("location")location:String= "",
        @Query("description")description:String = ""
    ):List<ListJobResponse>

    @GET("positions/{id}")
    suspend fun getDetailJob(@Path("id")id:String):DetailJobResponse
}