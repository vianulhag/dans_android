package com.test.myapplication.network.response

data class ListJobResponse(
    val id:String? = null,
    val title:String? = null,
    val company:String? = null,
    val company_logo:String? = null,
    val location:String? = null
)
