package com.test.myapplication.network.response

data class DetailJobResponse(
    val id:String? = null,
    val title:String? = null,
    val company:String? = null,
    val company_logo:String? = null,
    val location:String? = null,
    val type:String? = null,
    val company_url:String? = null,
    val description:String? = null
)
