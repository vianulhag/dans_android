package com.test.myapplication.view.list_job

import android.util.Log
import android.widget.Space
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.animateIntAsState
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.collectAsLazyPagingItems
import com.test.myapplication.R
import com.test.myapplication.component.CenterComponent
import com.test.myapplication.component.SearchField
import com.test.myapplication.navigation.NavRoute
import com.test.myapplication.network.response.ListJobResponse
import com.test.myapplication.widgets.ErrorNotification
import com.test.myapplication.widgets.FilterBox
import com.test.myapplication.widgets.JobItem

@Composable
fun ListJobView(
    listJobVm: ListJobViewModel,
    navController: NavController
) {

    val search = rememberSaveable {
        mutableStateOf("")
    }
    val dropDown = rememberSaveable {
        mutableStateOf(false)
    }
    val tempoIsFullTime = rememberSaveable {
        mutableStateOf(true)
    }
    val tempoLocation = rememberSaveable {
        mutableStateOf("")
    }

    val isFullTime = rememberSaveable {
        mutableStateOf(true)
    }
    val location = rememberSaveable {
        mutableStateOf("")
    }

    val animateChangeArrow by animateFloatAsState(targetValue = if (dropDown.value) -90f else 90f)

    listJobVm.getListJob(full_time = isFullTime.value, location = location.value, search.value)
    val jobState = listJobVm.jobState.collectAsLazyPagingItems()

    Column(
        Modifier
            .padding(14.dp)
    ) {
       CenterComponent {
           Text(
               text = "JOB LIST",
               fontSize = 16.sp,
               fontWeight = FontWeight.Bold
           )
       }

        Spacer(modifier = Modifier.height(14.dp))
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            SearchField(value = search.value, onValueChange = search)
            Spacer(modifier = Modifier.weight(1f))
            Icon(painter = painterResource(id = R.drawable.arrow_icon),
                contentDescription = null,
                modifier = Modifier
                    .rotate(animateChangeArrow)
                    .size(20.dp)
                    .clickable { dropDown.value = !dropDown.value })
        }
        AnimatedVisibility(visible = dropDown.value) {
            Column {
                Spacer(modifier = Modifier.height(10.dp))
                FilterBox(checked = tempoIsFullTime, location =  tempoLocation) {
                    isFullTime.value = tempoIsFullTime.value
                    location.value = tempoLocation.value
                    dropDown.value = false
                }
            }
        }
        Spacer(modifier = Modifier.height(14.dp))


        LazyColumn {
            items(jobState.itemCount) { index ->
                jobState[index]?.let { jobResponse ->
                    JobItem(jobResponse) {
                       try {
                           navController.navigate(NavRoute.DetailJob.route+"/${jobResponse.id}")
                       } catch (e:Exception) {
                           Log.e("Wrong Navigation",e.toString())
                       }
                    }
                    Spacer(modifier = Modifier.height(18.dp))
                }
            }
            jobState.apply {
                when {
                    loadState.refresh is LoadState.Loading || loadState.append is LoadState.Loading -> {
                        item {
                            CenterComponent {
                                Spacer(modifier = Modifier.height(12.dp))
                                CircularProgressIndicator()
                            }
                        }
                    }

                    loadState.refresh is LoadState.Error || loadState.append is LoadState.Error -> {
                        item {
                            Spacer(modifier = Modifier.height(12.dp))
                            ErrorNotification()
                            Spacer(modifier = Modifier.height(12.dp))
                        }
                    }

                }
            }
        }
    }
}