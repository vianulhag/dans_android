package com.test.myapplication.view.login

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.Firebase
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.auth
import com.test.myapplication.utils.Const
import com.test.myapplication.utils.CustSharePrefs
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val prefs: CustSharePrefs
) :
    ViewModel() {

    fun signInWithGoogle(activity: Activity) {
        val signOption = GoogleSignInOptions
            .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(Const.WEB_CLIENT_ID)
            .requestEmail()
            .build()

        val googleSignClick = GoogleSignIn.getClient(context, signOption)
        val getSignin = googleSignClick.signInIntent
        activity.startActivityForResult(getSignin, Const.RC_SIGN_IN.toInt())
    }

    fun firebaseAuthWithGoogle(auth: FirebaseAuth, token: String?, activity: Activity,login: () -> Unit) {
        val credential = GoogleAuthProvider.getCredential(token, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = auth.currentUser
                    Log.d("NAMANYA: ", "${user?.displayName}")
                    Toast.makeText(
                        context,
                        "Berhasil Login",
                        Toast.LENGTH_LONG
                    ).show()
                    login.invoke()
                    prefs.saveName(user?.displayName).let {
                        Log.d("Nama yang tersimpan", prefs.getName())
                    }

                } else {
                    Toast.makeText(
                        context,
                        "Login Gagal",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }


    fun loginWithFacebook(
        coroutineScope: CoroutineScope,
        auth: FirebaseAuth,
        activity: Activity,
        callbackManager: CallbackManager,
        login:() -> Unit
    ) {
        val loginManager = LoginManager.getInstance()
        loginManager.logInWithReadPermissions(activity, listOf("public_profile"))

        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                val credential: AuthCredential =
                    FacebookAuthProvider.getCredential(result.accessToken.token)
                coroutineScope.launch {
                    try {
                        val authResult = auth.signInWithCredential(credential).await()
                        val user: FirebaseUser? = authResult.user
                        Toast.makeText(context,
                            "Berhasil Login",
                            Toast.LENGTH_LONG).show()
                        login.invoke()

                        prefs.saveName(user?.displayName).let {
                            Log.d("Nama yang tersimpan", prefs.getName())
                        }
                    } catch (e: Exception) {
                        Log.e("ERROR EXCEPTION", e.toString())
                    }
                }
            }
            override fun onError(error: FacebookException) {
                Log.e("ERROR FACEBOOK", error.toString())
            }

            override fun onCancel() {
                Log.e("CANCEL FACEBOOK", "error.toString()")
            }
        })
    }

    fun getLogin():String = prefs.getName()

}