package com.test.myapplication.view.profile

import androidx.lifecycle.ViewModel
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import com.test.myapplication.utils.CustSharePrefs
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(private val prefs: CustSharePrefs):ViewModel() {

    fun getName():String = prefs.getName()

    fun logOut() {
        Firebase.auth.signOut()
    }

    fun setName(name:String) = prefs.saveName(name)


}