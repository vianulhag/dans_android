package com.test.myapplication.view.list_job

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.test.myapplication.network.response.ListJobResponse
import com.test.myapplication.repositories.ListJobRepository
import com.test.myapplication.utils.ListJobPagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListJobViewModel @Inject constructor(
    private val repo: ListJobRepository) : ViewModel() {
    private val _jobState = MutableStateFlow<PagingData<ListJobResponse>>(PagingData.empty())
    val jobState = _jobState.asStateFlow()

    fun getListJob(
        full_time: Boolean = true,
        location: String,
        description: String
    ) = viewModelScope.launch {
        val jobPaging = ListJobPagingSource(
            repo = repo,
            full_time = full_time,
            location = location,
            description = description)
        Pager(
            config = PagingConfig(
                pageSize = 10
            )
        ) {
            jobPaging
        }.flow.cachedIn(viewModelScope).collect {
            pagingData ->
            _jobState.value = pagingData
        }
    }
}