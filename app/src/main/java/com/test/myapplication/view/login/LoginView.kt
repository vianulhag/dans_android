package com.test.myapplication.view.login

import android.app.Activity
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.facebook.CallbackManager
import com.google.firebase.auth.FirebaseAuth
import com.test.myapplication.component.ButtonLogin
import com.test.myapplication.R
import com.test.myapplication.component.CenterComponent
import com.test.myapplication.navigation.NavRoute

@Composable
fun LoginView(
    loginVm: LoginViewModel,
    activity: Activity,
    auth: FirebaseAuth,
    callbackManager: CallbackManager,
    navController:NavController
) {
    val coroutineScope = rememberCoroutineScope()

    Column(
        Modifier
            .padding(14.dp)
            .fillMaxSize()
            .wrapContentSize(Center)
    ) {

        CenterComponent {
            Text(
                text = "Welcome User",
                fontWeight = FontWeight.Bold,
                fontSize = 14.sp
            )
        }

        Spacer(modifier = Modifier.height(16.dp))
//        Login Facebook
        ButtonLogin(
            color = Color(0xFF316FF6),
            icon = R.drawable.logo_fb,
            text = "Continue With Facebook",
            fontColor = Color.White
        ) {
            try {
                loginVm.loginWithFacebook(
                    coroutineScope = coroutineScope,
                    auth = auth,
                    activity,
                    callbackManager
                ) {
                    navController.navigate(NavRoute.Profile.route) {
                        popUpTo(navController.graph.id) {
                            inclusive = true
                        }
                    }
                }
            } catch (e: Exception) {
                Log.e("ERROR ClOSE", e.toString())
            }
        }
        Spacer(modifier = Modifier.height(16.dp))
        CenterComponent {
            Text(
                text = "OR",
                fontWeight = FontWeight.Light,
                fontSize = 14.sp
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
//        Login Google
        ButtonLogin(
            color = Color(0xFFFFFFFF),
            icon = R.drawable.logo_google,
            text = "Sign In With Google",
            fontColor = Color.Gray
        ) {
            try {
                loginVm.signInWithGoogle(activity)
            } catch (e: Exception) {
                Log.e("ERROR ClOSE", e.toString())
            }
        }


    }
}