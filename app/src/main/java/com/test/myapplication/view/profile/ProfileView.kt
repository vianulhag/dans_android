package com.test.myapplication.view.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.test.myapplication.component.CenterComponent
import com.test.myapplication.component.PhotoProfileWrapper
import com.test.myapplication.R
import com.test.myapplication.component.ButtonLogin
import com.test.myapplication.navigation.NavRoute

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProfileView(
    profileVm:ProfileViewModel,
    navController: NavController
) {
    Scaffold {
        Surface (
            modifier = Modifier
                .padding(it)
                .fillMaxSize()
        ) {

                Column (
                    Modifier
                        .padding(14.dp)
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.height(18.dp))
                    Text(
                        text = "ACCOUNT",
                        fontWeight = FontWeight.Bold,
                        fontSize = 14.sp
                    )

                    Spacer(modifier = Modifier.height(18.dp))


                    PhotoProfileWrapper {
                        Image(painter = painterResource(id = R.drawable.profile),
                            contentDescription = null,
                            contentScale = ContentScale.Crop)
                    }
                    Spacer(modifier = Modifier.height(14.dp))
                    Text(
                        text = profileVm.getName(),
                        fontWeight = FontWeight.Bold,
                        fontSize = 16.sp
                    )
                    Spacer(modifier = Modifier.height(18.dp))
                    ButtonLogin(color = Color.Red,
                        icon = R.drawable.logout_icon ,
                        text = "Logout ",
                        fontColor = Color.White,
                        fraction = 0.5f) {
                        profileVm.logOut()
                        profileVm.setName("").let {
                            navController.navigate(NavRoute.Login.route) {
                                popUpTo(navController.graph.id) {
                                    inclusive = true
                                }
                            }
                        }
                    }
                }


        }
    }
}
