package com.test.myapplication.view.detail_job

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.myapplication.network.response.DetailJobResponse
import com.test.myapplication.repositories.DetailJobRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailJobViewModel @Inject constructor(private val repo:DetailJobRepository):ViewModel() {
    private val _detailState = MutableStateFlow(DetailJobResponse())
    val detailState = _detailState.asStateFlow()

    fun getDetailJob(id:String) =
        viewModelScope.launch {
            try {
                repo.getDetailJob(id).let { list ->
                    _detailState.value = list
                }
            } catch (e:Exception) {
                Log.e("Detail Error",e.toString())
            }
        }
}