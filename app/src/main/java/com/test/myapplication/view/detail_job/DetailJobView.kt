package com.test.myapplication.view.detail_job

import android.widget.Space
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.test.myapplication.R
import com.test.myapplication.widgets.CompanyDetail
import com.test.myapplication.widgets.JobSpecification

@Composable
fun DetailJobView(
    navController: NavController,
    id:String,
    detailVm:DetailJobViewModel
) {

    detailVm.getDetailJob(id)
    val detailState = detailVm.detailState.collectAsState().value

    Column(
        Modifier
            .padding(14.dp)
    ) {
        Row (
            Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Icon(painter = painterResource(id = R.drawable.arrow_icon),
                contentDescription = null,
                modifier = Modifier
                    .rotate(180f)
                    .size(18.dp)
                    .clickable { navController.popBackStack() })
            Text(text = "JOB DETAIL",
                fontWeight = FontWeight.Bold,
                fontSize = 16.sp)
            Icon(painter = painterResource(id = R.drawable.arrow_icon),
                contentDescription = null,
                modifier = Modifier
                    .rotate(180f)
                    .size(18.dp)
                    .clickable { navController.popBackStack() },
                tint = Color.Transparent)
        }
        Spacer(modifier = Modifier.height(12.dp))
        Text(text = "Company",
            fontWeight = FontWeight.Bold,
            fontSize = 14.sp)
        Spacer(modifier = Modifier.height(6.dp))
        CompanyDetail(item = detailState)

        Spacer(modifier = Modifier.height(12.dp))
        Text(text = "Job Specification",
            fontWeight = FontWeight.Bold,
            fontSize = 14.sp)
        Spacer(modifier = Modifier.height(6.dp))
        JobSpecification(item = detailState)

    }
}