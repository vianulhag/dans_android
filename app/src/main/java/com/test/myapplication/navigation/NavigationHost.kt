package com.test.myapplication.navigation

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.facebook.CallbackManager
import com.google.firebase.auth.FirebaseAuth
import com.test.myapplication.view.detail_job.DetailJobView
import com.test.myapplication.view.detail_job.DetailJobViewModel
import com.test.myapplication.view.list_job.ListJobView
import com.test.myapplication.view.list_job.ListJobViewModel
import com.test.myapplication.view.login.LoginView
import com.test.myapplication.view.login.LoginViewModel
import com.test.myapplication.view.profile.ProfileView
import com.test.myapplication.view.profile.ProfileViewModel

@Composable
fun NavigationHost(
    activity: Activity,
    auth: FirebaseAuth,
    callbackManager: CallbackManager,
    navController: NavHostController,
    showBackground:MutableState<Boolean>
) {

    val loginVm = hiltViewModel<LoginViewModel>()
    val profileVm = hiltViewModel<ProfileViewModel>()
    val listJobVm = hiltViewModel<ListJobViewModel>()
    val detailVm = hiltViewModel<DetailJobViewModel>()

    NavHost(
        navController = navController,
        startDestination = if (loginVm.getLogin()
                .isEmpty()
        ) NavRoute.Login.route else NavRoute.Profile.route
    ) {
        composable(NavRoute.Login.route) {
            showBackground.value = false
            LoginView(
                loginVm = loginVm,
                activity = activity,
                auth = auth,
                callbackManager = callbackManager,
                navController
            )
        }

        composable(NavRoute.Profile.route) {
            showBackground.value = true
            ProfileView(
                profileVm = profileVm,
                navController = navController
            )
        }

        composable(NavRoute.ListJob.route) {
            showBackground.value = true
            ListJobView(listJobVm = listJobVm,navController)
        }

        composable(
            NavRoute.DetailJob.route + "/{id}",
            arguments = listOf(
                navArgument("id") {
                    type = NavType.StringType
                }
            )
        ) {
            showBackground.value = false
            DetailJobView(
                navController = navController,
                id = it.arguments?.getString("id")!!,
                detailVm = detailVm
            )
        }
    }
}