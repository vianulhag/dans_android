package com.test.myapplication.navigation

import com.test.myapplication.R

sealed class NavRoute(val route:String,val icon:Int = 0) {
    object Login:NavRoute("login")
    object Profile:NavRoute("profile", R.drawable.profile_icon)
    object ListJob:NavRoute("list_job", R.drawable.home_icon)
    object DetailJob:NavRoute("detail_job")
}
