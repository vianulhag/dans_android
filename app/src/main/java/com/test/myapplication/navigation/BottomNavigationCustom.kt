package com.test.myapplication.navigation

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState

@Composable
fun BottomnNavigationCustom(navController: NavController) {

    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route
    BottomAppBar(
        containerColor = Color.White
    ) {
        Row(
            Modifier
                .padding(start = 30.dp, end = 30.dp)
                .fillMaxWidth(),
            Arrangement.SpaceBetween
        ) {
            NavigationItem(
                navController,
                NavRoute.ListJob.route,
                NavRoute.ListJob.icon,
                "Home",
                color = if (currentRoute == NavRoute.ListJob.route) Color.Black else Color.Black.copy(
                    0.5f
                )
            )
            Text(
                text = "|",
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold
            )
            NavigationItem(
                navController,
                NavRoute.Profile.route,
                NavRoute.Profile.icon,
                "Account",
                color = if (currentRoute == NavRoute.Profile.route) Color.Black else Color.Black.copy(
                    0.5f
                )
            )
        }
    }
}

@Composable
fun NavigationItem(
    navController: NavController,
    route: String,
    icon: Int,
    label: String,
    color: Color
) {
    IconButton(
        onClick = {
            navController.navigate(route) {
                navController.graph.startDestinationRoute?.let { screen_route ->
                    popUpTo(screen_route) {
                        saveState = true
                    }
                }
                launchSingleTop = true
                restoreState = true
            }
        }
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = label,
            modifier = Modifier
                .size(20.dp),
            tint = color
        )
    }
}